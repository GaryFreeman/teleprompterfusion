﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;
using System.Windows;

namespace TelePrompterFusionLib
{
    // Параграф
    // Блок, состоящий из вложенных строчных элементов (span'ов, изображений и т.д.)
    public class Paragraph : Block
    {
        private ObservableCollection<Inline> inlines = new ObservableCollection<Inline>();

        public Paragraph(string text, Block parent)
        {
            if (text != null)
            {
                Inlines.Add(new Span(text, new Font("Arial", 22)));
            }
            Parent = parent;
            inlines.CollectionChanged += InlinesCollectionChanged;
        }

        public Paragraph(string text)
            : this(text, null)
        {
        }

        public Paragraph(Block parent)
            : this(null, parent)
        {
        }

        public Paragraph()
            : this(null, null)
        {
        }

        void InlinesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove ||
                e.Action == NotifyCollectionChangedAction.Replace ||
                e.Action == NotifyCollectionChangedAction.Reset)
            {
                foreach (Inline inl in e.OldItems)
                {
                    Span s = inl as Span;
                    if (s != null)
                    {
                        s.SpanInvalidated -= SpanInvalidated;
                    }
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Add ||
                e.Action == NotifyCollectionChangedAction.Replace ||
                e.Action == NotifyCollectionChangedAction.Reset)
            {
                foreach (Inline inl in e.NewItems)
                {
                    Span s = inl as Span;
                    if (s != null)
                    {
                        s.SpanInvalidated += SpanInvalidated;
                    }
                }
            }

            Invalidate();
        }

        private void SpanInvalidated(object sender, EventArgs e)
        {
            Invalidate();
        }

        // Строчные элементы параграфа
        public ObservableCollection<Inline> Inlines
        {
            get
            {
                return inlines;
            }
        }

        public override IEnumerable<TextElement> Children
        {
            get
            {
                return (IEnumerable<TextElement>)inlines;
            }
        }

        public override void Mesure(TelePrompter tp)
        {
            if (isMesured)
            {
                return;
            }

            //if (Inlines.Count == 0)
            //{
            //    throw new InvalidOperationException("Can't mesure paragraph: paragraph doesn't contain inlines.");
            //}

            if (tp == null)
            {
                throw new ArgumentNullException("tp");
            }

            this.tp = tp;
            System.Windows.Size oldSize = Dimensions.Size;
            System.Windows.Size newSize = new System.Windows.Size(tp.Width, 0);

            if (Inlines.Count != 0)
            {
                using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
                {
                    // Сколько осталось пикселей до конца строки
                    int spaceLeft = tp.Width;
                    // Ширина строки
                    int lineWidth = tp.Width;
                    // Указатель на текущую позицию в строке
                    int currentLinePos = 0;
                    // Начало текущей строки (в пикселях)
                    int lineStartX = 0;
                    // Начало текущей строки (позиция в строке)
                    int lineStartPos = 0;
                    // Высота текущей строки
                    int currentLineHeight = 0;

                    foreach (Inline inl in Inlines)
                    {
                        // Пробегаем по всем строчным элементам

                        // Для запоминания позиции строчного элемента в параграфе
                        Rect inlinePosition = new Rect(lineStartX, newSize.Height, 0, 0);

                        Span s = inl as Span;
                        if (s != null)
                        {
							s.TelePrompter = tp;

                            // Если элемент - фрагмент форматированного текста
                            // (а пока мы кроме этого ничего и не поддерживаем)

                            lineStartPos = 0;

                            // Ширина пробела
                            int spaceWidth = MesureStringWidth(" ", s.Font, g);
                            // Индекс текущей позиции (для запоминания мест переноса)
                            int currentPosition = 0;
                            // Линии переноса
                            List<SpanLine> wrapLines = new List<SpanLine>();

                            foreach (string w in SplitAndKeep(s.Text, new char[] { ' ' }))
                            {
                                // Пробегаем по всем словам

                                // Ширина слова в пикселях
                                int wordWidth = MesureStringWidth(w, s.Font, g);
                                //if (w[w.Length - 1] == ' ')
                                //{
                                //    wordWidth -= spaceWidth;
                                //}

                                if (wordWidth > spaceLeft && spaceLeft >= 0)
                                {
                                    // Если слово не помещается в строку,
                                    // переносим слово, увеличиваем высоту параграфа,
                                    // запоминаем место переноса внутри фрагмента

                                    if (w == " ")
                                    {
                                        // Не переносим пробел на новую строку

                                        currentPosition += w.Length;
                                        continue;
                                    }

                                    if (currentPosition - lineStartPos != 0)
                                    {
                                        // Если хоть сколько-то текущего span'а было отрисовано на этой строке

                                        wrapLines.Add(new SpanLine() {
                                            Start = lineStartPos,
                                            Length = currentPosition - lineStartPos,
                                            Dimensions = new Rect(
                                                lineStartX,
                                                newSize.Height,
                                                currentLinePos - lineStartX,
                                                s.LineHeight
                                            )
                                        });
                                    }

                                    spaceLeft = lineWidth - wordWidth;
                                    
                                    currentLinePos = wordWidth;

                                    // Обновляем координату в строке и координату на экране следующей строки
                                    lineStartX = 0;
                                    lineStartPos = currentPosition;
                                    newSize.Height += currentLineHeight;

                                    // Обновляем ширину span'а
                                    inlinePosition.X = 0;
                                    inlinePosition.Width = lineWidth;
                                    inlinePosition.Height += currentLineHeight;

                                    if (currentLineHeight > s.LineHeight)
                                    {
                                        currentLineHeight = s.LineHeight;
                                    }
                                }
                                else
                                {
                                    // Если же слово вмещается, или не вмещается,
                                    // но начинается от начала строки - уменьшаем
                                    // оставшееся до конца строки место
                                    spaceLeft -= wordWidth;
                                    currentLinePos += wordWidth;

                                    if (inlinePosition.Width < lineWidth)
                                    {
                                        inlinePosition.Width += wordWidth;
                                    }
                                }

                                if (s.LineHeight > currentLineHeight)
                                {
                                    // Если высота фрагмента больше высоты текущей строки,
                                    // Обновляем высоту текущей строки

                                    currentLineHeight = s.LineHeight;
                                }
                                currentPosition += w.Length;
                            }

                            // Запоминаем последнюю строку span'а
                            wrapLines.Add(new SpanLine() { 
                                Start = lineStartPos,
                                Length = currentPosition - lineStartPos,
                                Dimensions = new Rect(
                                    lineStartX,
                                    newSize.Height,
                                    currentLinePos - lineStartX,
                                    s.LineHeight
                                )
                            });
                            lineStartX = currentLinePos;
                            lineStartPos = currentPosition;
                            s.Lines = wrapLines;

                            // Запоминаем высоту span'а (учитывая переносы)
                            inlinePosition.Height += s.LineHeight;
                            s.Dimensions = inlinePosition;
                        }
                    }

                    newSize.Height += currentLineHeight;
                }
            }

            isMesured = true;

            Dimensions = new Rect(Dimensions.Location, newSize);
            if (newSize != oldSize)
            {
                OnSizeChanged(
                    this,
                    new SizeChangedEventArgs() { OldSize = oldSize, NewSize = newSize }
                );
            }
        }

        // Возвращает ширину строки
        protected int MesureStringWidth(string text, Font font, Graphics g)
        {
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            StringFormat format = StringFormat.GenericTypographic;
            format.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces;

            return (int)Math.Round(g.MeasureString(text, font, int.MaxValue, format).Width, MidpointRounding.AwayFromZero);
        }

        // Разбивает строку (аналог String.Split), сохраняя разделитель
        public static IEnumerable<string> SplitAndKeep(string s, char[] delims)
        {
            int start = 0, index;

            while ((index = s.IndexOfAny(delims, start)) != -1)
            {
                if (index - start > 0)
                    yield return s.Substring(start, index - start);
                yield return s.Substring(index, 1);
                start = index + 1;
            }

            if (start < s.Length)
            {
                yield return s.Substring(start);
            }
        }

        public override string ToString()
        {
            return "Paragraph";
        }
    }
}
