﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace TelePrompterFusionLib
{
    // Блок раздела
    public abstract class Block : TextElement
    {
        protected bool isMesured = false;

        public event SizeChangedEventHandler SizeChanged;

        public Block()
        {
        }

        // см. описание Rundown::Mesure
        public abstract void Mesure(TelePrompter tp);

        // Необходим пересчет блока
        protected override void Invalidate()
        {
            if (tp != null)
            {
                isMesured = false;
                Mesure(tp);
            }
        }

        protected void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (SizeChanged != null)
            {
                SizeChanged(sender, e);
            }
        }
    }
}
