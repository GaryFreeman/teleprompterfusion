﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;
using System.Windows;
using NLog;

namespace TelePrompterFusionLib
{
    // Форматированная строка
    public class Span : Inline
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string text;
        private int lineHeight = 0;
        private List<SpanLine> lines = new List<SpanLine>();

        public event EventHandler SpanInvalidated;

        public Span(string text, Font font)
        {
            this.text = text;
            Font = font;
        }

        // Текст строки
        public string Text
        {
            get
            {
                return text;
            }
        }

        // Высота строки
        public int LineHeight
        {
            get
            {
                if (lineHeight == 0)
                {
                    using (FontFamily ff = new FontFamily(Font.Name))
                    {
                        int emHeight = ff.GetEmHeight(Font.Style);
                        float fontSize = Font.Size;

                        if (Font.Unit == GraphicsUnit.Point)
                        {
                            using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
                            {
                                fontSize = Font.Size * (1F / 72F) * g.DpiY;
                            }
                        }

                        int lineSpacing = ff.GetLineSpacing(Font.Style);
                        float lineSpacingInPixels = fontSize * lineSpacing / ff.GetEmHeight(Font.Style);
                        lineHeight = (int)Math.Round(lineSpacingInPixels, MidpointRounding.AwayFromZero);
                    }
                }

                return lineHeight;
            }
        }

        // Линии переноса span'а
        public List<SpanLine> Lines
        {
            get
            {
                return lines;
            }

            set
            {
                lines = value;
            }
        }

		public TelePrompter TelePrompter
		{
			get
			{
				return tp;
			}

			set
			{
				tp = value;
			}
		}

        public override IEnumerable<TextElement> Children
        {
            get
            {
                return (IEnumerable<TextElement>)Lines;
            }
        }

        public override Bitmap Render(Rect drawRect)
        {
            var currentLevel = level++;
            logger.Debug("[{0}] Начинаем отрисовку span'а \"{1}\".", currentLevel, this.ToString());
            logger.Debug("[{0}] Позиция span'а: {1}.", currentLevel, Dimensions.ToString());
            logger.Debug("[{0}] Позиция отрисовки: {1}.", currentLevel, drawRect.ToString());

            Bitmap b = new Bitmap((int)drawRect.Size.Width, (int)drawRect.Size.Height);

            if (Lines.Count != 0)
            {
                // Делаем "хит-тестинг": определяем, какие линии надо отрисовать
                bool atLeastOneFound = false;

                using (Graphics g = Graphics.FromImage(b))
                {
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                    StringFormat format = StringFormat.GenericTypographic;
                    format.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces;

                    logger.Debug("[{0}] Начинаем цикл по всем строкам.", currentLevel);
                    foreach (SpanLine line in Lines)
                    {
                        var lineRelativeRect = line.Dimensions;
                        lineRelativeRect.X -= Dimensions.X;
                        lineRelativeRect.Y -= Dimensions.Y;

                        logger.Debug("[{0}] Строка: \"{1}\".", currentLevel, line.ToString());
                        logger.Debug("[{0}] Позиция строки в строке: {1}.", currentLevel, line.Dimensions);
                        logger.Debug("[{0}] Позиция строки относительно span'а: {1}.", currentLevel, lineRelativeRect);

                        Rect intersection = FindIntersection(drawRect, lineRelativeRect);

                        if (intersection != Rect.Empty && intersection.Width != 0 &&
                            intersection.Height != 0)
                        {
                            // Линию необходимо отрисовать

                            logger.Debug("[{0}] Строку необходимо отрисовать: она входит в область отрисовки.", currentLevel);

                            atLeastOneFound = true;

                            Rect localDrawRect = lineRelativeRect;
                            localDrawRect.X -= drawRect.X;
                            localDrawRect.Y -= drawRect.Y;

                            logger.Debug("[{0}] Область отрисовки: {1}.", currentLevel, localDrawRect.ToString());

                            g.DrawString(
                                Text.Substring(line.Start, line.Length),
                                Font,
                                new SolidBrush(tp.Foreground),
                                (float)localDrawRect.Location.X,
                                (float)localDrawRect.Location.Y,
                                format
                            );
                        }
                        else
                        {
                            logger.Debug("[{0}] Нет необходимости отрисовывать строку: она не пересекается с областью отрисовки.", currentLevel);

                            if (atLeastOneFound)
                            {
                                //break;
                            }
                        }
                    }
                    logger.Debug("[{0}] Завершили цикл по строкам.", currentLevel);
                }
            }

            logger.Debug("[{0}] Отрисовка завершена.", currentLevel);
            --level;

            return b;
        }

        protected override void Invalidate()
        {
            if (SpanInvalidated != null)
            {
                SpanInvalidated(this, new EventArgs());
            }
        }

        public override string ToString()
        {
            return "Span";
        }
    }
}
