﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TelePrompterFusionLib
{
    // Линия переноса span'а
    public struct SpanLine
    {
        // Индекс начала перенесенной строки
        public int Start
        {
            get;
            set;
        }

        // Длина перенесенной строки
        public int Length
        {
            get;
            set;
        }

        // Позиция и размер перенесенной строки
        public Rect Dimensions
        {
            get;
            set;
        }

        public override string ToString()
        {
            return "SpanLine";
        }
    }
}
