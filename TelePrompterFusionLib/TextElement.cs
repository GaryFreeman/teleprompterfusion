﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows;
using NLog;

namespace TelePrompterFusionLib
{
    public abstract class TextElement : IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected TextElement parentTextElement = null;
        protected Font ownFont = null;
        protected TelePrompter tp = null;
        protected Rect dimensions = new Rect(0, 0, 0, 0);
        protected static int level = 0;

        // Шрифт
        public virtual Font Font
        {
            get
            {
                if (ownFont != null)
                {
                    return ownFont;
                }
                else if (parentTextElement != null)
                {
                    return parentTextElement.Font;
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (ownFont != null)
                {
                    ownFont.Dispose();
                }

                ownFont = value;
                Invalidate();
            }
        }

        // Родительский элемент
        public virtual TextElement Parent
        {
            get
            {
                return parentTextElement;
            }

            set
            {
                parentTextElement = value;

                if (ownFont == null)
                {
                    Invalidate();
                }
            }
        }

        // Потомки элемента
        public abstract IEnumerable<TextElement> Children
        {
            get;
        }

        // Позиция и размеры элемента внутри родителя
        public Rect Dimensions
        {
            get
            {
                return dimensions;
            }

            set
            {
                dimensions = value;
            }
        }

		public virtual Bitmap Render(Rect drawRect)
		{
			return Render(null, drawRect);
		}

        // Отрисовать указанную область
        public virtual Bitmap Render(Bitmap bitmap, Rect drawRect)
        {
            var currentLevel = level++;
            logger.Debug("[{0}] Начинаем отрисовку элемента \"{1}\".", currentLevel, this.ToString());
            logger.Debug("[{0}] Позиция элемента: {1}.", currentLevel, Dimensions.ToString());
            logger.Debug("[{0}] Позиция отрисовки: {1}.", currentLevel, drawRect.ToString());

            if (tp == null)
            {
                throw new InvalidOperationException("Can't render element: element isn't attached to TelePrompter.");
            }

            Bitmap b = new Bitmap((int)drawRect.Size.Width, (int)drawRect.Size.Height);

            // Делаем "хит-тестинг": определяем, какие элементы надо отрисовать
            bool atLeastOneFound = false;

            using (Graphics g = Graphics.FromImage(b))
            {
                logger.Debug("[{0}] Начинаем цикл по всем потомкам.", currentLevel);
                foreach (TextElement el in Children)
                {
                    logger.Debug("[{0}] Потомок: \"{1}\".", currentLevel, el.ToString());
                    logger.Debug("[{0}] Позиция потомка: {1}.", currentLevel, el.Dimensions.ToString());

                    Rect intersection = FindIntersection(drawRect, el.Dimensions);

                    if (intersection != Rect.Empty && intersection.Width != 0 &&
                        intersection.Height != 0)
                    {
                        // Блок необходимо отрисовать

                        logger.Debug("[{0}] Потомка необходимо отрисовать: он входит в область отрисовки.", currentLevel);

                        atLeastOneFound = true;

                        Rect localDrawRect = FindLocalDrawRect(drawRect, el.Dimensions);
                        Rect localRenderRect = FindLocalRenderRect(localDrawRect, drawRect,
                            el.Dimensions);

                        logger.Debug("[{0}] Область отображения потомка: {1}.", currentLevel, localDrawRect.ToString());
                        logger.Debug("[{0}] Область отрисовки потомка: {1}.", currentLevel, localRenderRect.ToString());

                        g.DrawImage(
                            el.Render(localRenderRect),
                            new PointF(
                                (float)localDrawRect.Location.X,
                                (float)localDrawRect.Location.Y
                            )
                        );
                    }
                    else
                    {
                        logger.Debug("[{0}] Нет необходимости отрисовывать потомка: он не пересекается с областью отрисовки.", currentLevel);

                        if (atLeastOneFound)
                        {
                            break;
                        }
                    }
                }
                logger.Debug("[{0}] Завершили цикл по потомкам.", currentLevel);
            }

            logger.Debug("[{0}] Отрисовка завершена.", currentLevel);
            --level;

            return b;
        }

        // Возвращает пересечение двух областей
        protected Rect FindIntersection(Rect rect1, Rect rect2)
        {
            Rect result = rect1;

            result.Intersect(rect2);

            return result;
        }

        // Находит область, в которую необходимо отрисовать изображение потомка
        protected Rect FindLocalDrawRect(Rect drawRect, Rect childDim)
        {
            Rect localDrawRect = drawRect;
            localDrawRect.Intersect(childDim);
            localDrawRect.X -= drawRect.X;
            localDrawRect.Y -= drawRect.Y;
            return localDrawRect;
        }

        // Находит область, которую необходимо отрисовать внутри потомка, т.е. drawRect для потомка
        protected Rect FindLocalRenderRect(Rect localDrawRect, Rect drawRect, Rect childDim)
        {
            Rect localRenderRect = localDrawRect;
            localRenderRect.Intersect(new Rect(0, 0, drawRect.Width, drawRect.Height));
            localRenderRect.X -= childDim.X - drawRect.X;
            localRenderRect.Y -= childDim.Y - drawRect.Y;
            return localRenderRect;
        }

        // Состояние элемента изменилось (например, необходим перерасчет)
        protected abstract void Invalidate();

        public void Dispose()
        {
            if (ownFont != null)
            {
                ownFont.Dispose();
            }
        }
    }
}
