﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelePrompterFusionLib
{
    public class PositionChangedEventArgs : EventArgs
    {
        public int OldPosition
        {
            get;
            set;
        }

        public int NewPosition
        {
            get;
            set;
        }
    }
}
