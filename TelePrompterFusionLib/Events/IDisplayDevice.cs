﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TelePrompterFusionLib
{
    // Устройство отображение суфлера
    public interface IDisplayDevice
    {
        // Этот метод вызывается суфлером при появлении нового кадра.
        // Внимание! Он вызывается не из GUI-потока.
        void ReceiveFrame(Bitmap frame);
    }
}
