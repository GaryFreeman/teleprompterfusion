﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TelePrompterFusionLib
{
    public class SizeChangedEventArgs : EventArgs
    {
        public Size OldSize
        {
            get;
            set;
        }

        public Size NewSize
        {
            get;
            set;
        }
    }
}
