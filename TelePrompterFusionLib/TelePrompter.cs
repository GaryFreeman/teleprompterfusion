﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using System.Windows;
using NLog;

namespace TelePrompterFusionLib
{
    // Главный класс суфлера
	public class TelePrompter : IDisposable
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private int width = 800;
        private int height = 600;
        private Section source;
        private Color background = Color.Black;
        private Color foreground = Color.Yellow;
        private double position = 0;
        private bool isRunning = false;
        private int speed = 0;
        private object speedMutex = new Object();
		private object isRunningMutex = new Object();
        private ManualResetEvent wh = new ManualResetEvent(false);
        private Thread thread;
		private ObservableCollection<IDisplayDevice> devices = new ObservableCollection<IDisplayDevice>();
        private RenderedFrame lastFrame;
        private Object lastFrameMutex = new Object();
		private double pointerPosition = 0.25;

        public event PositionChangedEventHandler PositionChanged;

        public TelePrompter()
        {
            logger.Info("Начало создания.");
            thread = new Thread(GameLoop);
            thread.IsBackground = true;
            thread.Start();
            logger.Info("TelePrompter создан, поток суфлера запущен.");

			Source = new Rundown();
			devices.CollectionChanged += DevicesChanged;
        }

        // Ширина экрана суфлера
		public int Width
		{
			get
            {
                return width;
            }

			set
            {
                width = value;
            }
		}

        // Высота экрана суфлера
        public int Height
		{
			get
            {
                return height;
            }

			set
            {
                height = value;
            }
		}

        // Источник (скрипт или очередь)
        public Section Source
		{
			get
            {
                return source;
            }

			set
            {
                logger.Info("Изменение источника.");

                Stop();

                lock (lastFrameMutex)
                {
                    lastFrame = RenderedFrame.Empty;
                }

                source = value;
                source.Mesure(this);

                GoToPosition(0);
            }
		}

        // Указатель текущей позиции суфлера
        public double Position
        {
            get
            {
                return position;
            }

            private set
            {
                logger.Info("Изменение позиции: {0}.", value);

                position = value;
            }
        }

        // Цвет фона
        public Color Background
        {
            get
            {
                return background;
            }

            set
            {
                background = value;
            }
        }

        // Основной цвет текста
        public Color Foreground
        {
            get
            {
                return foreground;
            }

            set
            {
                foreground = value;
            }
        }

        // Запущен ли суфлер
        public bool IsRunning
        {
            get
            {
				lock (isRunningMutex) {
					return isRunning;				
				}
            }

            set
            {
				lock (isRunningMutex) {
					if (isRunning != value) {
						isRunning = value;

						logger.Info("Суфлер " + ((isRunning) ? "запущен" : "остановлен") + ".", IsRunning);

						if (isRunning) {
							if (Speed != 0) {
								wh.Set();
							}
						} else {
							wh.Reset();
						}
					}
				}
            }
        }

        // Скорость (в пикселях в секунду)
        public int Speed
        {
            get
            {
                lock (speedMutex)
                {
                    return speed;
                }
            }

            set
            {
                lock (speedMutex)
                {
                    speed = value;

                    logger.Info("Изменена скорость: {0}.", speed);

                    if (speed == 0)
                    {
                        wh.Reset();
                    }
                    else
                    {
                        if (IsRunning)
                        {
                            wh.Set();
                        }
                    }
                }
            }
        }

        // Устройства отображения
        public IList<IDisplayDevice> Devices
        {
            get
            {
                return devices;
            }
        }

		// Позиция указателя (стрелки). 0 - верх экрана, 1 - низ
		public double PointerPosition
		{
			get
			{
				return pointerPosition;
			}

			set
			{
				pointerPosition = value;
			}
		}

        // Запустить суфлер
        public void Start()
        {
            IsRunning = true;
        }

        // Остановить суфлер
        public void Stop()
        {
            IsRunning = false;
        }

        // Ручной переход к конкретной позиции
        public void GoToPosition(int position)
        {
            logger.Info("Ручной переход к позиции {0}.", position);

            Stop();
            Position = position;
            Render(0);
        }

        // Обновить устройства
        private void UpdateDevices(Bitmap frame)
        {
            logger.Debug("Обновляем устройства отображения.");

            foreach (var device in Devices)
            {
                logger.Debug("Обновление устройства отображения \"{0}\".", device.ToString());
                device.ReceiveFrame(frame);
            }

            logger.Debug("Все устройства отображения обновлены.");
        }

        // "Игровой цикл" суфлера. В цикле вызывает обновление состояния и перерисовку
        private void GameLoop()
        {
            const int TICKS_PER_SECOND = 25;
            const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
            const int MAX_FRAMESKIP = 5;

            int nextGameTick = Environment.TickCount;
            int loops;
            float interpolation;

            while (true)
            {
                wh.WaitOne();

                loops = 0;
                while (Environment.TickCount > nextGameTick && loops < MAX_FRAMESKIP)
                {
                    UpdateState();

                    nextGameTick += SKIP_TICKS;
                    ++loops;
                }

                interpolation = ((float)(Environment.TickCount + SKIP_TICKS - nextGameTick)) / (float)SKIP_TICKS;
                Render(interpolation);
            }
        }

        // Обновление состояния
        private void UpdateState()
        {
            int oldPosition = (int)Position;
            double offset = speed / 25.0;

			if (Position + offset < Source.Dimensions.Height - Height * PointerPosition &&
				Position + offset > -(Height * PointerPosition)) {
				logger.Debug("Обновляем состояние: сдвигаем позицию на {0}.", offset);
				Position += offset;
				logger.Debug("Текущая позиция: {0}.", Position);

				if (oldPosition != (int)Position && PositionChanged != null) {
					var eventArgs = new PositionChangedEventArgs();
					eventArgs.OldPosition = oldPosition;
					eventArgs.NewPosition = (int)Position;
					PositionChanged(this, eventArgs);
				}
			} else {
				logger.Info("Суфлер достиг конца (начала). Останавливаем.");
				IsRunning = false;
			}
        }

        // Отрисовка
        private void Render(float interpolation)
        {
            logger.Debug("Начинаем перерисовку.");
            if (Devices.Count > 0)
            {
                // Если есть устройства, отрисовываем (иначе отрисовка не требуется)

                logger.Debug("Устройств отображения: {0}.", Devices.Count);

                Bitmap frame = new Bitmap(Width, Height);
                using (Graphics g = Graphics.FromImage(frame))
                {
                    logger.Debug("Начинаем непосредственно перерисовку.");
                    g.FillRectangle(new SolidBrush(Background), 0, 0, Width, Height);

                    var offset = -((float)speed / 25 * interpolation);
                    logger.Debug("Сдвиг с учетом предсказания: {0}.", offset);

                    Rect renderedRect = Rect.Empty;
                    Rect forRenderRect = new Rect(0, (int)Position, Width, Height);
                    Rect drawRect = new Rect(0, 0, Width, Height);

                    lock (lastFrameMutex)
                    {
                        if (lastFrame != RenderedFrame.Empty)
                        {
                            // Существует уже отрисованный предыдущий кадр
                            // (необязательно мы можем его использовать)

                            renderedRect = new Rect(0, lastFrame.Position, lastFrame.Bitmap.Width,
                                lastFrame.Bitmap.Height);
                            logger.Debug("Существует уже отрисованный кадр.");
                        }
                    
                        bool fullRender = false;
                        renderedRect.Intersect(forRenderRect);
                        if (renderedRect != Rect.Empty)
                        {
                            // Часть кадра уже отрисована в предыдущий раз

                            renderedRect.Y -= (int)Position;

                            if (renderedRect.Y == 0)
                            {
                                // Отрисованная область сверху

                                forRenderRect.Y += renderedRect.Height;
                                forRenderRect.Height -= renderedRect.Height;
                                drawRect.Y += renderedRect.Height;
                                drawRect.Height -= renderedRect.Height;
                            }
                            else if (renderedRect.Y == Height - renderedRect.Height)
                            {
                                // Отрисованная область снизу

                                forRenderRect.Height -= renderedRect.Height;
                                drawRect.Height -= renderedRect.Height;
                            }
                            else
                            {
                                // Отрисованная область посередине
                                // (отрисовываем полностью, т.к. пока не умеем такое обрабатывать)

                                fullRender = true;
                            }

                            renderedRect.Y = lastFrame.Position - (int)Position;
                        }
                        else
                        {
                            fullRender = true;
                        }

                        if (!fullRender)
                        {
                            logger.Debug("Часть кадра по координатам {0} уже отрисована. Выводим ее.",
                                renderedRect);
                            g.DrawImage(lastFrame.Bitmap, (int)renderedRect.X, (int)renderedRect.Y);
                        }
                        else
                        {
                            // Надо отрисовывать весь кадр полностью

                            logger.Debug("Отрисовываем весь кадр.");
                        }
                    }

                    if (forRenderRect.Height != 0)
                    {
                        logger.Debug("Отрисовываем оставшуюся часть ({0}).", forRenderRect);
                        Bitmap b = Source.Render(forRenderRect);
                        forRenderRect.Y -= (int)Position;
                        logger.Debug("Выводим оставшуюся часть по координатам {0}.", forRenderRect);
                        g.DrawImage(b, 0, (int)forRenderRect.Y);
                    }

                    logger.Debug("Перерисовка завершена.");
                }

                var frameForSave = frame.Clone(new Rectangle(0, 0, frame.Width, frame.Height), frame.PixelFormat);
                lock (lastFrameMutex)
                {
                    lastFrame = new RenderedFrame(frameForSave, (int)Position);
                }
                logger.Debug("Сохраняем последний кадр с позицией {0}.", (int)Position);

				DrawPositionPointer(frame);

                UpdateDevices(frame);
            }
            else
            {
                logger.Debug("Перерисовка не требуется: нет устройств для отображения.");
            }
        }

		private void DrawPositionPointer(Bitmap frame)
		{
			using (Graphics g = Graphics.FromImage(frame)) {
				g.DrawLine(new Pen(Foreground), 0, (int)(Height * PointerPosition), Width, (int)(Height * PointerPosition));
			}
		}

		private void DevicesChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (!IsRunning) {
				Render(0);
			}
		}

        public void Dispose()
        {
            thread.Abort();
            wh.Dispose();
        }

	}
}

