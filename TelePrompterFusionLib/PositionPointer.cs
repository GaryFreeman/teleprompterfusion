﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelePrompterFusionLib
{
    // Указатель позиции суфлера
    // Имеет две характеристики: текущий скрипт и
    // смещение относительно текущего скрипта.
    public struct PositionPointer
    {
        public Script Script
        {
            get;
            set;
        }

        public int Offset
        {
            get;
            set;
        }
    }
}
