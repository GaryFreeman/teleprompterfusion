﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;
using System.Drawing;

namespace TelePrompterFusionLib
{
    // Раздел скрипта
    // Блок, который содержит в себе набор других суб-блоков
    public class Section : Block
    {
        private ObservableCollection<Block> blocks = new ObservableCollection<Block>();

        public Section()
        {
            blocks.CollectionChanged += SubBlocksCollectionChanged;
        }

        // При изменении набора суб-блоков
        private void SubBlocksCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove ||
                e.Action == NotifyCollectionChangedAction.Replace ||
                e.Action == NotifyCollectionChangedAction.Reset)
            {
                foreach (Block b in e.OldItems)
                {
                    b.SizeChanged -= SubBlockSizeChanged;
                    b.Parent = null;
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Add ||
                e.Action == NotifyCollectionChangedAction.Replace ||
                e.Action == NotifyCollectionChangedAction.Reset)
            {
                foreach (Block b in e.NewItems)
                {
                    b.SizeChanged += SubBlockSizeChanged;
                    b.Parent = this;
                }
            }

            Invalidate();
        }

        // При изменении высоты одного из суб-блоков
        private void SubBlockSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!isMesured)
            {
                return;
            }

            System.Windows.Size oldSize = Dimensions.Size;
            System.Windows.Size newSize = Dimensions.Size;

            newSize.Height -= e.OldSize.Height;
            newSize.Height += e.NewSize.Height;

            Dimensions = new Rect(Dimensions.Location, newSize);
            if (oldSize != newSize)
            {
                OnSizeChanged(
                    this,
                    new SizeChangedEventArgs() { OldSize = oldSize, NewSize = newSize }
                );
            }
        }

        // Блоки секции
        public ObservableCollection<Block> Blocks
        {
            get
            {
                return blocks;
            }
        }

        public override IEnumerable<TextElement> Children
        {
            get
            {
                return (IEnumerable<TextElement>)blocks;
            }
        }

        // см. описание Rundown::Mesure
        public override void Mesure(TelePrompter tp)
        {
            if (isMesured)
            {
                return;
            }

            /*if (Blocks.Count == 0)
            {
                throw new InvalidOperationException("Can't mesure section: section doesn't contain blocks.");
            }*/

            if (tp == null)
            {
                throw new ArgumentNullException("tp");
            }

            this.tp = tp;
            System.Windows.Size oldSize = Dimensions.Size;
            System.Windows.Size newSize = new System.Windows.Size(0, 0);
            double maxWidth = 0;

            if (Blocks.Count != 0)
            {
                foreach (Block b in Blocks)
                {
                    b.Mesure(tp);
                    b.Dimensions = new Rect(0, newSize.Height, b.Dimensions.Size.Width, b.Dimensions.Size.Height);
                    newSize.Height += b.Dimensions.Size.Height;
                    if (b.Dimensions.Size.Width > maxWidth)
                    {
                        maxWidth = b.Dimensions.Size.Width;
                    }
                }

                newSize.Width = maxWidth;
            }
            isMesured = true;

            Dimensions = new Rect(Dimensions.Location, newSize);
            if (oldSize != newSize)
            {
                OnSizeChanged(
                    this,
                    new SizeChangedEventArgs() { OldSize = oldSize, NewSize = newSize }
                );
            }
        }

        public override string ToString()
        {
            return "Section";
        }
    }
}
