﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TelePrompterFusionLib
{
    // Информация о последнем отрисованном кадре
    struct RenderedFrame
    {
        public static readonly RenderedFrame Empty = new RenderedFrame();

        private Bitmap bitmap;
        private int position;

        public RenderedFrame(Bitmap bitmap, int position)
        {
            if (bitmap == null)
            {
                throw new ArgumentNullException("bitmap");
            }

            this.bitmap = bitmap;
            this.position = position;
        }

        public static bool operator ==(RenderedFrame first, RenderedFrame second)
        {
            return first.Bitmap == second.Bitmap && first.Position == second.Position;
        }

        public static bool operator !=(RenderedFrame first, RenderedFrame second)
        {
            return !(first == second);
        }

        public override int GetHashCode()
        {
            return Bitmap.GetHashCode() + Position;
        }

        public override bool Equals(object obj)
        {
            return obj is RenderedFrame && this == (RenderedFrame)obj;
        }

        public Bitmap Bitmap
        {
            get
            {
                return bitmap;
            }

            set
            {
                bitmap = value;
            }
        }

        public int Position
        {
            get
            {
                return position;
            }

            set
            {
                position = value;
            }
        }
    }
}
