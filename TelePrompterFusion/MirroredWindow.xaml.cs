﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;
using TelePrompterFusionLib;

namespace TelePrompterFusion
{
    /// <summary>
    /// Interaction logic for MirroredWindow.xaml
    /// </summary>
    public partial class MirroredWindow : Window, IDisplayDevice
    {
        public MirroredWindow()
        {
            InitializeComponent();
        }

        public void ReceiveFrame(Bitmap frame)
        {
            Dispatcher.BeginInvoke((Action)delegate()
            {
                var copyFrame = frame.Clone(new System.Drawing.Rectangle(0, 0, frame.Width, frame.Height),
                    frame.PixelFormat);
                copyFrame.RotateFlip(RotateFlipType.Rotate180FlipY);
                using (MemoryStream memory = new MemoryStream())
                {
                    copyFrame.Save(memory, ImageFormat.Png);
                    memory.Position = 0;
                    BitmapImage bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();

                    Image.Source = bitmapImage;
                }
            });
        }
    }
}
