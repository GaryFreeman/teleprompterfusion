﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;
using TelePrompterFusionLib;

namespace TelePrompterFusion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TelePrompter tp = new TelePrompter();

        public MainWindow()
        {
            InitializeComponent();

            MirroredWindow mirrored = new MirroredWindow();
            tp.Devices.Add(mirrored);
            mirrored.Show();

            tp.Speed = 0;
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            tp.Stop();
            string text = Text.Text;
            string[] lines = text.Split('\n');
            Script s = new Script();
            foreach (var line in lines)
            {
                s.Blocks.Add(new Paragraph(line));
            }
            tp.Source = s;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            tp.Start();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            tp.Stop();
        }

        private void MinusSpeed_Click(object sender, RoutedEventArgs e)
        {
            tp.Speed -= 1;
        }

        private void PlusSpeed_Click(object sender, RoutedEventArgs e)
        {
            tp.Speed += 1;
        }
    }
}
